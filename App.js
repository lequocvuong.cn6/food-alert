import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Login from './components/login/login';
import Home from './Home';

const Stack = createStackNavigator();

const App = () => (
  <NavigationContainer>
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
      <Stack.Screen name="Map" component={Home} options={{ headerShown: false }} />

    </Stack.Navigator>
  </NavigationContainer>

);

// make this component available to the app
export default App;
