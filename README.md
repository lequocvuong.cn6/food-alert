# FloodAlert
Please consult our [Wiki](https://github.com/TranscendEngineering/FloodAlert/wiki) to get started. 

## Team members
Karim Hasbini 40053498 <br>
Jennifer Younanian 40003712<br>
Gregory Khchadourian 40064648<br>
Vincent Cerri 40034135<br>
Garo Balouzian 40065652<br>
Raffi Jansezian 40063156<br>
Victor Manea 40001995<br>
Brinda Mailvaganam 40029586<br>
Manpreet Sohal 40057219<br>
