import React from 'react';
import { Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SearchScreen from './components/search/SearchScreen';
import Map from './components/map/map';
import Watchlist from './components/watchlist/watchlist';
import Settings from './components/settings/settings';

function HomeScreen({ route, navigation }) {
  return (
    <Map navigation={navigation} route={route} />
  );
}

function WatchlistScreen({ navigation }) {
  return (
    <Watchlist navigation={navigation} />
  );
}

function AlertsScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Alerts!</Text>
    </View>
  );
}

// function SettingsScreen() {
//  return (
//    <Settings />
//  );
// }

export default class Home extends React.Component {
  render() {
    return (
      <NavigationContainer independent>
        <Tab.Navigator
          initialRouteName="SearchScreen"
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;

              if (route.name === 'Map') {
                iconName = 'md-map';
              } else if (route.name === 'Settings') {
                iconName = focused
                  ? 'ios-list-box'
                  : 'ios-list';
              } else if (route.name === 'Search') {
                iconName = 'ios-search';
              } else if (route.name === 'Watchlist') {
                iconName = 'md-bookmark';
              } else if (route.name === 'Alerts') {
                iconName = focused
                  ? 'ios-notifications'
                  : 'ios-notifications-outline';
              }

              // You can return any component that you like here!
              return <Ionicons name={iconName} size={size} color={color} />;
            },
          })}
          tabBarOptions={{
            activeTintColor: '#5E96AE',
            inactiveTintColor: 'gray',
            activeBackgroundColor: '#B2EBE0',
          }}
        >
          <Tab.Screen name="Search" component={SearchScreen} />
          <Tab.Screen name="Map" component={HomeScreen} />
          <Tab.Screen name="Watchlist" component={WatchlistScreen} />
          <Tab.Screen name="Alerts" component={AlertsScreen} />
          <Tab.Screen name="Settings" component={Settings} />
        </Tab.Navigator>
      </NavigationContainer>
    );
  }
}

const Tab = createBottomTabNavigator();
