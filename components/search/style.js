import { StyleSheet } from 'react-native';

export default StyleSheet.create({

  statesListHeader: {
    borderColor: 'lightgrey',
    borderTopWidth: 1,
  },

  siteListHeader: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: 'white',
    borderColor: 'lightgrey',
    borderTopWidth: 1,
  },

  headerTitle: {
    backgroundColor: 'white',
    fontWeight: 'bold',
    color: 'black',
    padding: '2.5%',
    fontSize: 35,
  },

  clearIcon: {
    justifyContent: 'center',
    padding: '2.5%',
  },

  searchArea: {
    height: 125,
    justifyContent: 'flex-end',
    backgroundColor: 'white',
    paddingHorizontal: '2.5%',
  },

  loading: {
    padding: '5%',
    justifyContent: 'center',
    alignItems: 'center',
  },

});
