import * as React from 'react';
import {
  Text, View, FlatList, ActivityIndicator,
} from 'react-native';
import { SearchBar, ListItem, Icon } from 'react-native-elements';
import * as Location from 'expo-location';
import { states } from '../../data/states';
import styles from './style';


/**
 *  The SearchScreen class component allows the user to search for a particular US state (or select a state from the displayed list of all the states).
 *  Selecting a state will return the list of its sites (name + site code).
 */

export default class SearchScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
      queriedStates: [],
      error: null,
      loaded: true,
      displayResults: false,
      header: '',
    };
  }

  componentDidMount() {
    this.setState({ loaded: false });
    this.loadStateList();
    this.selectCurrentState();
  }

  handleSearch = (query) => {
    this.setState((prevState) => ({
      queriedStates: prevState.inMemoryStates.filter(
        (item) => {
          const statesLowercase = item.name.toLowerCase();
          const searchLowercase = query.toLowerCase();
          return statesLowercase.indexOf(searchLowercase) > -1;
        },
      ),
      query,
    }));
  }

  loadStateList = () => {
    this.setState({
      loaded: true,
      queriedStates: states,
      inMemoryStates: states,
    });
  }

  getData = (abbr) => {
    // const baseURL = 'https://waterservices.usgs.gov/nwis/gwlevels/?format=json&siteStatus=active&stateCd=';
    const baseURL = `https://waterservices.usgs.gov/nwis/iv/?format=json&stateCd=${abbr}&siteStatus=active`;

    this.setState({
      loaded: false,
      error: null,
    });
    // const url = baseURL + abbr;
    const req = new Request(baseURL, {
      method: 'GET',
    });

    fetch(req)
      .then((response) => response.json())
      .then(this.showData)
      .catch(this.errorCaught);
  }

  showData = (results) => {
    this.setState({
      loaded: true,
      displayResults: true,
      data: results.value.timeSeries,
    });
  }

  errorCaught = (err) => {
    this.setState({
      loaded: true,
      error: err.message,
    });
  }

  renderStates = ({ item }) => (
    <ListItem onPress={() => {
      this.getData(item.abbreviation); this.setHeader(item.name);
    }}
    >
      <ListItem.Content>
        <ListItem.Title>{item.name}</ListItem.Title>
        <ListItem.Subtitle>{item.abbreviation}</ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron />
    </ListItem>

  )

  navigateMap = (item) => {
    // eslint-disable-next-line no-console
    console.log('navigateMap');
    // console.log(item);
    // console.log(item.sourceInfo.geoLocation.geogLocation.latitude);
    // console.log(item.sourceInfo.siteName);

    const obj = {};
    obj.siteName = item.sourceInfo.siteName;
    obj.lat = item.sourceInfo.geoLocation.geogLocation.latitude;
    obj.lon = item.sourceInfo.geoLocation.geogLocation.longitude;
    obj.siteNumber = item.sourceInfo.siteCode[0].value;
    this.props.navigation.navigate('Map', { item: obj });
  }

  renderResults = ({ item }) => (
    <ListItem onPress={() => this.navigateMap(item)}>
      <ListItem.Content>
        <ListItem.Title>{item.sourceInfo.siteName}</ListItem.Title>
        <ListItem.Subtitle>{item.sourceInfo.siteCode[0].value}</ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron />
    </ListItem>
  )

  statesListHeader = () => (
    <View style={styles.statesListHeader}>
      <Text style={styles.headerTitle}>
        States
      </Text>
    </View>
  )

  siteListHeader = () => (
    <View style={styles.siteListHeader}>
      <Text style={styles.headerTitle}>
        {`Sites: ${this.state.header}`}
      </Text>
      <View style={styles.clearIcon}>
        <Icon
          name="clear-all"
          onPress={this.clearResults}
        />
      </View>
    </View>
  )

  clearResults = () => {
    this.setState({
      displayResults: false,
    });
  }

  setHeader = (selectedState) => {
    this.setState({
      header: selectedState,
    });
  }

  // if user's location services are turned on, the state user is located in is selected and gages found in that state are shown
  // if user location is outside usa, no state is selected
  async selectCurrentState() {
    try {
      const { status } = await Location.requestPermissionsAsync();
      if (status === 'granted') {
        const location = await Location.getCurrentPositionAsync({});
        const coordinates = {
          latitude: location.coords.latitude, // 38.933066, no state would be selected as we are testing from Canada
          longitude: location.coords.longitude, // -88.138572, for testing purposes uncomment and use these coordinates to set the location to illinois as user's state
        };

        const result = await Location.reverseGeocodeAsync(coordinates);
        const userRegion = result[0].region[0].toUpperCase() + result[0].region.slice(1); // result[0].region;
        const stateReturned = states.filter((item) => item.name === userRegion);
        console.log(userRegion);

        if (stateReturned !== undefined && stateReturned[0] !== undefined) {
          const stateName = stateReturned[0].name;
          const stateAbbr = stateReturned[0].abbreviation;
          this.getData(stateAbbr);
          this.setHeader(stateName);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.searchArea}>
          <SearchBar
            placeholder="Search"
            onChangeText={this.handleSearch}
            value={this.state.query}
            platform="ios"
          />
        </View>

        { !this.state.loaded && (
          <View style={styles.loading}>
            <ActivityIndicator size="large" />
            <Text style={{ padding: '3%' }}>This might take a while...</Text>
          </View>
        )}

        { (this.state.loaded && this.state.displayResults) && (
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={this.state.data}
          renderItem={this.renderResults}
          ListHeaderComponent={this.siteListHeader}
          stickyHeaderIndices={[0]}
        />
        )}

        { (this.state.error && !this.state.loaded) && (
          <Text>{this.state.error}</Text>
        )}

        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={this.state.queriedStates}
          renderItem={this.renderStates}
          ListHeaderComponent={this.statesListHeader}
          stickyHeaderIndices={[0]}
        />
      </View>
    );
  }
}
