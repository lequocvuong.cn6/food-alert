import React, { Component } from 'react';
import {
  View, Text, Image, TouchableOpacity, ImageBackground,
} from 'react-native';
import styles from './style';
import { CredentialsFields } from '../utilities/utilities';

const userinfo = { username: 'Admin', password: 'Admin' };
const tinyLogo = require('../../assets/splash.png');
const background = require('../../assets/background.png');

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  getCredentials = (credentials) => {
    this.setState({
      username: credentials.username,
      password: credentials.password,
    });
  }

  Login=async () => {
    if (userinfo.username === this.state.username && userinfo.password === this.state.password) {
      this.props.navigation.navigate('Map');
    } else {
      alert('Username or password incorrect');
    }
  }

  render() {
    return (

      <View style={{ flex: 1 }}>

        <ImageBackground
          style={styles.imgBackground}
          resizeMode="cover"
          source={background}
        >

          <View style={styles.main}>
            <Image source={tinyLogo} style={styles.tinyLogo} />
            <Text style={styles.title}>Flood Alert</Text>
            <CredentialsFields
              parentCredentials={this.getCredentials}
            />

            <View>
              <TouchableOpacity
                style={styles.primaryButton}
                onPress={() => this.Login()}
              >
                <Text>
                  Login
                </Text>
              </TouchableOpacity>

            </View>

          </View>
          <View style={styles.footer}>
            <TouchableOpacity style={styles.secondaryButton}>
              <Text style={styles.specialText}>
                Forget Login Details
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.secondaryButton}
            >
              <Text style={styles.specialText}>
                Create Account
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.secondaryButton}
              onPress={() => this.props.navigation.navigate('Map')}
            >
              <Text style={styles.specialText}>Skip Login</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>

      </View>

    );
  }
}

export default Login;
