/* eslint-disable no-underscore-dangle */
// disable _array default sqlite
import React from 'react';
import MapView, { Marker } from 'react-native-maps';
import {
  View, Animated, SafeAreaView, TouchableOpacity,
} from 'react-native';
import * as Location from 'expo-location';
import { Button } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import * as SQLite from 'expo-sqlite';
import styles from './style';

import ZoomSlider from './ZoomSlider';
import DraggableView from './DraggableView';
import FilterMenu from '../filter/FilterMenu';

const db = SQLite.openDatabase('database.db');
/* lat Delta and lang delta help determine the zoom level for MapView */
const latitudeDelta = 0.0922;
const longitudeDelta = latitudeDelta;
class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sites: [],
      siteName: 'No gage has been selected',
      siteRegion: '',
      siteDischarge: '0',
      siteGauge: '0',

      /* sets region displayed on map.
         Change this to move to a new region.
         This does not change when moving the map or zooming in-out the map.
      */
      region: {
        latitude: 45.536060,
        longitude: -73.657548,
        latitudeDelta,
        longitudeDelta,
      },
      modalVisible: false,
      itemPoi: {
        lat: 0,
        lon: 0,
        siteName: '',
      },
      isFavor: false,
    };
  }

  async componentDidMount() {
    this.setCurrentLocation();
    this.filterMap('');
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      // do something
      console.log('focus Map');
      this.initData();
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onBookMark() {
    this.setState((preState) => ({
      isFavor: !preState.isFavor,
    }));

    if (!this.state.isFavor) {
      let check = false;
      db.transaction(
        (tx) => {
          tx.executeSql(
            'create table if not exists watchlist (id integer primary key not null, siteName text, lat text, lon text, siteNumber text);',
          );
          tx.executeSql('select * from watchlist', [], (_, { rows }) => {
            console.log(JSON.stringify(rows));

            for (let i = 0; i < rows._array.length; i += 1) {
              if (rows._array[i].siteName === this.state.itemPoi?.siteName) {
                check = true;
              }
            }
            console.log(`check: ${check}`);
            if (!check) {
              tx.executeSql('insert into watchlist (siteName, lat, lon, siteNumber) values (?, ?, ?, ?)', [this.state.itemPoi.siteName, this.state.itemPoi.lat, this.state.itemPoi.lon, this.state.itemPoi.siteNumber]);
            } else {
              alert('Cannot add same Poi in watchlist');
            }
          });
        },
        (e) => {
          console.log(e);
        },
      );
    }
  }

  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible });
  };

  /*
    if user's location services are on, will move map to the user's location
    otherwise will ask for permissions.
  */
  async setCurrentLocation() {
    try {
      const { status } = await Location.requestPermissionsAsync();
      if (status !== 'granted') {
        return;
      }
      const location = await Location.getCurrentPositionAsync({});

      this.setState((prevState) => ({
        region: {
          latitude: location.coords.latitude,
          longitude: location.coords.longitude,
          latitudeDelta: prevState.region.latitudeDelta,
          longitudeDelta: prevState.region.longitudeDelta,
        },
      }));
    } catch (error) {
      // console.log(error);
    }
  }

  async getCamCenter() {
    let center;
    await this.mapRef.getCamera().then((cam) => {
      center = cam.center;
    });
    return center;
  }

  // eslint-disable-next-line react/sort-comp
  updateIndex = (selectedIndex) => {
    // eslint-disable-next-line react/no-unused-state
    this.setState({ selectedIndex });
  }

  // Pull Discharge and Gauge value from API
  getDischargeAndGageHeight(value) {
    // eslint-disable-next-line no-console
    console.log(`sites number: ${value}`);
    fetch(
      `https://waterservices.usgs.gov/nwis/iv/?format=json&sites=${value}&parameterCd=00060`,
      {
        method: 'GET',
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {
        // Logic for representing Discharge value retrieved via API
        if (responseJson.value.timeSeries != null && responseJson.value.timeSeries.length > 0) {
          if (responseJson.value?.timeSeries[0]?.values[0]?.value[0]?.value < 0) {
            this.setState({ siteDischarge: 'N/A' });
          } else {
            this.setState({ siteDischarge: responseJson.value?.timeSeries[0]?.values[0]?.value[0]?.value });
          }
        } else {
          this.setState({ siteDischarge: 'N/A' });
        }
        // eslint-disable-next-line no-console
        console.log(this.state.siteDischarge);
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error);
      });

    fetch(
      `https://waterservices.usgs.gov/nwis/iv/?format=json&sites=${value}&parameterCd=00065`,
      {
        method: 'GET',
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {
        // Logic for representing Gauge value retrieved via API
        if (responseJson.value.timeSeries != null && responseJson.value.timeSeries.length > 0) {
          if (responseJson.value?.timeSeries[0]?.values[0]?.value[0]?.value < 0) {
            this.setState({ siteGauge: 'N/A' });
          } else {
            this.setState({ siteGauge: responseJson.value?.timeSeries[0]?.values[0]?.value[0]?.value });
          }
        } else {
          this.setState({ siteGauge: 'N/A' });
        }
        // eslint-disable-next-line no-console
        console.log(this.state.siteGauge);
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error);
      });
  }

  filterMap = (filterString) => {
    // eslint-disable-next-line no-console
    console.log('filterMap');
    // State to populate with markers
    const stateName = 'VT';
    this.state.sites = [];

    // Fetch data points from USGS site
    fetch(
      `https://waterservices.usgs.gov/nwis/iv/?format=json&stateCd=${stateName}&siteStatus=active${filterString}`,
      {
        method: 'GET',
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {
        // Assign value to values based on state sites
        this.setState((prevState) => {
          const values = [...prevState.sites];
          values.push(...responseJson.value.timeSeries);
          return ({ sites: values });
        });

        // eslint-disable-next-line no-console
        console.log(this.state.sites.length);
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error);
      });
    // }
  }

  splitName = (nameUpper) => {
    const name = nameUpper.toLowerCase().split(' ').map((word) => word.charAt(0).toUpperCase() + word.substring(1)).join(' ');

    if (name.includes(' At ') && name.includes(' Near ')) {
      const nameSplit = name.split(/At|Near/);

      this.setState({ siteName: nameSplit[0] });
      this.setState({ siteRegion: nameSplit[2] });
    } else if (name.includes(' At ') && name.includes(' Nr ')) {
      const nameSplit = name.split(/At|Nr/);

      this.setState({ siteName: nameSplit[0] });
      this.setState({ siteRegion: nameSplit[2] });
    } else if (name.includes(' Abv ') && name.includes(' Nr ')) {
      const nameSplit = name.split(/Abv|Nr/);

      this.setState({ siteName: nameSplit[0] });
      this.setState({ siteRegion: nameSplit[2] });
    } else if (name.includes(' Abv ') && name.includes(' Near ')) {
      const nameSplit = name.split(/Abv|Near/);

      this.setState({ siteName: nameSplit[0] });
      this.setState({ siteRegion: nameSplit[2] });
    } else if (name.includes(' Above ') && name.includes(' At ')) {
      const nameSplit = name.split(/Above|At/);

      this.setState({ siteName: nameSplit[0] });
      this.setState({ siteRegion: nameSplit[2] });
    } else if (name.includes(' Blw ') && name.includes(' At ')) {
      const nameSplit = name.split(/Blw|At/);

      this.setState({ siteName: nameSplit[0] });
      this.setState({ siteRegion: nameSplit[2] });
    } else if (name.includes(' @ ') && name.includes(' Nr ')) {
      const nameSplit = name.split(/@|Nr/);

      this.setState({ siteName: nameSplit[0] });
      this.setState({ siteRegion: nameSplit[2] });
    } else if (name.includes(' Near ')) {
      const nameSplit = name.split(' Near ');

      this.setState({ siteName: nameSplit[0] });
      this.setState({ siteRegion: nameSplit[1] });
    } else if (name.includes(' Nr ')) {
      const nameSplit = name.split(' Nr ');

      this.setState({ siteName: nameSplit[0] });
      this.setState({ siteRegion: nameSplit[1] });
    } else if (name.includes(' At ')) {
      const nameSplit = name.split(' At ');

      this.setState({ siteName: nameSplit[0] });
      this.setState({ siteRegion: nameSplit[1] });
    } else if (name.includes(' @ ')) {
      const nameSplit = name.split(' @ ');

      this.setState({ siteName: nameSplit[0] });
      this.setState({ siteRegion: nameSplit[1] });
    } else if (name.includes(' Below ')) {
      const nameSplit = name.split(' Below ');

      this.setState({ siteName: nameSplit[0] });
      this.setState({ siteRegion: nameSplit[1] });
    } else {
      this.setState({ siteName: name });
    }
  }

  getitemPoi = (item) => {
    /**
     * Method to retrieve and set the itemPoi values from a sites information when tapping the marker
     * @param {Object} item - The gage information pulled from the USGS API
     */

    // Update the itemPoi to the Marker's information
    this.setState((prevState) => {
      const itemPoi = { ...prevState.itemPoi };
      itemPoi.lat = item.sourceInfo.geoLocation.geogLocation.latitude;
      itemPoi.lon = item.sourceInfo.geoLocation.geogLocation.longitude;
      itemPoi.siteName = item.sourceInfo.siteName;
      return { itemPoi };
    });
  }

  checkModalVisibility = (visible) => {
    this.setState({ modalVisible: visible });
  }

  calculateStartingSliderValue() {
    return Math.sqrt(Math.log(360 / this.state.region.longitudeDelta) / Math.LN2);
  }

  initData() {
    this.setState({
      region: {
        latitude: 45.536060,
        longitude: -73.657548,
        latitudeDelta,
        longitudeDelta,
      },
      itemPoi: {
        lat: 0,
        lon: 0,
        siteName: '',
      },
      isFavor: false,
      siteDischarge: '0',
      siteGauge: '0',
    });
    console.log(this.props);
    const item = this.props.route.params?.item;
    if (item) {
      // eslint-disable-next-line no-console
      console.log(item);
      this.setState((prevState) => ({
        region: {
          latitude: Number(item.lat),
          longitude: Number(item.lon),
          latitudeDelta: prevState.region.latitudeDelta,
          longitudeDelta: prevState.region.longitudeDelta,
        },
        itemPoi: item,
      }));
      this.getDischargeAndGageHeight(item.siteNumber);
      this.splitName(item.siteName);
    }
  }

  updateRegion(region) {
    this.setState({ region });
  }

  // updates state latitudeDelta and longitudeDelta as zoom is controlled by these two attributes
  async updateZoomLevel(value) {
    const latLongCoef = this.state.region.latitudeDelta / this.state.region.longitudeDelta;
    let zoom = Math.log(360 / this.state.region.longitudeDelta) / Math.LN2; // find current zoom level

    zoom += value; // add zoom delta (slider value)

    // calulate new latDelta and longDelta
    const power = Math.log2(360 / this.state.region.longitudeDelta) - zoom;
    const newLongitudeDelta = 2 ** power;
    const newLatitudeDelta = newLongitudeDelta * latLongCoef;

    // current camera position
    const camPosition = await this.getCamCenter();

    // create region to animate and animate the camera to that location
    const newRegion = {
      latitude: camPosition.latitude,
      longitude: camPosition.longitude,
      latitudeDelta: newLatitudeDelta,
      longitudeDelta: newLongitudeDelta,
    };

    this.mapRef.animateToRegion(newRegion, 1000);
  }

  render() {
    let pullDownMenu;
    // eslint-disable-next-line prefer-const
    pullDownMenu = (
      <DraggableView name={this.state.siteName} region={this.state.siteRegion} discharge={this.state.siteDischarge} gauge={this.state.siteGauge} style={{ flex: 1 }} />
    );
    return (
      <Animated.View style={{ flex: 1 }}>
        <SafeAreaView>
          <MapView
            style={styles.map}
            showsUserLocation
            followsUserLocation
            loadingEnabled
            region={this.state.region}
            ref={(ref) => {
              (this.mapRef = ref);
            }}
          >
            { // Place markers on the specified coordinates
          this.state.sites.map((item, index) => ( // look here this is a list,  react wants a key for everyhing in a list
            <Marker
              key={index.toString()} // Anything unique should work here like the index number since it wont rpeat in the list
              coordinate={
                {
                  latitude: item.sourceInfo.geoLocation.geogLocation.latitude,
                  longitude: item.sourceInfo.geoLocation.geogLocation.longitude,
                }
              }
              // Provide a title based off title from the site
              title={item.sourceInfo.siteName}
              // on Marker press, the site name, region, measurements, and coordinates are set to be displayed.
              onPress={() => {
                this.splitName(item.sourceInfo.siteName); this.getDischargeAndGageHeight(item.sourceInfo.siteCode[0].value); this.getitemPoi(item);
              }}
            />
          ))
          }

          </MapView>

          {/* This method decides which header to display at the top of the map */}
          {pullDownMenu}

          <View style={styles.locationButtonContainer}>
            <Button
              className="button"
              onPress={() => {
                this.setCurrentLocation();
              }}
              buttonStyle={{ backgroundColor: 'transparent' }}
              icon={(
                <Ionicons
                  name="md-locate"
                  size={30}
                  color="#81b8ad"
                />
            )}
            />
          </View>

          <View style={styles.filterButtonContainer}>
            <Button
              onPress={() => {
                this.checkModalVisibility(true);
              }}
              buttonStyle={{ backgroundColor: 'transparent' }}
              icon={(
                <Ionicons
                  name="md-options"
                  size={32}
                  color="#81b8ad"
                />
            )}
            />
          </View>

          {/* This is the bookmark icon */}
          <View style={styles.bookmarkButtonContainer}>
            <TouchableOpacity onPress={() => this.onBookMark()}>
              <FontAwesome
                name={this.state.isFavor ? 'bookmark' : 'bookmark-o'}
                size={30}
                color="#CC9966"
              />
            </TouchableOpacity>
          </View>
          <FilterMenu isVisible={this.state.modalVisible} visibilityCallback={this.checkModalVisibility} filterCallback={this.filterMap} />
          <ZoomSlider updateZoomLevel={(val) => this.updateZoomLevel(val)} startingZoom={this.calculateStartingSliderValue(longitudeDelta)} />

        </SafeAreaView>
      </Animated.View>
    );
  }
}

export default Map;
