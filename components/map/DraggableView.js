import 'react-native-gesture-handler';
import * as React from 'react';
import {
  Animated, Dimensions, PanResponder, View, Text, Image,
} from 'react-native';
import styles from './style';

const { height } = Dimensions.get('window'); // obtains the height of the window
const initialHeight = (height / 4) - height; // portion of the height of the view that is displayed initially
const heightOpen = (-height / 8); // draggable view height when dragged down
const graphImage = require('../../assets/flowchart_placeholder.png'); // this image is a placeholder that is used within the pull down menu. Will be replaced with a functioning graph later.

class DraggableView extends React.Component {
  constructor(props) {
    super(props);
    this.animation = new Animated.ValueXY({ x: 0, y: initialHeight }); // y sets portion of the height of the view that is displayed initially
    this.state = {
      shouldDragDown: true, // Boolean value that decides whether the menu is pulled up or down.
      isHidden: 1.0, // Opacity value for showing/hidding objects in the menu.
    };

    this.panResponder = PanResponder.create({
      onMoveShouldSetPanResponder: () => true,

      // Animating the pull down action on the menu.
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dy < 0 && !this.state.shouldDragDown) {
          this.state.shouldDragDown = true;
          Animated.spring(this.animation.y, {
            toValue: initialHeight,
            tension: 1,
            useNativeDriver: true,
          }).start();
          this.setState({ isHidden: 1.0 });
        } else if (gestureState.dy > 0 && this.state.shouldDragDown) {
          this.state.shouldDragDown = false;
          Animated.spring(this.animation.y, {
            toValue: heightOpen,
            tension: 1,
            useNativeDriver: true,
          }).start();
          this.setState({ isHidden: 0.0 });
        }
      },
    });
  }

  render() {
    const animatedViewHeight = { transform: this.animation.getTranslateTransform() };

    return (
      <Animated.View
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...this.panResponder.panHandlers}
        style={[animatedViewHeight, {
          flex: 1,
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          backgroundColor: 'white',
          height,
        }]}
      >
        <Animated.View
          style={styles.animatedView}
        >
          <View style={{
            flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white',
          }}
          >
            {/* The following view is what is shown when the menu is pulled down. */}
            <View>
              <View style={styles.riverInfoContainer}>
                <Text style={styles.riverNameHeader}>{this.props.name}</Text>
                <Text style={styles.riverLocationHeader}>{this.props.region}</Text>
                <View style={[{ flexDirection: 'row', alignItems: 'center' }]}>
                  <View style={[{ flex: 1, flexDirection: 'row' }]}>
                    <View style={[{ flex: 1, flexDirection: 'column' }]}>
                      <Text style={styles.riverStatsHeader}>Discharge (ft3/s):</Text>
                      <Text style={styles.riverStatsHeader}>{this.props.discharge}</Text>
                    </View>
                  </View>
                  <View style={[{ justifyContent: 'space-evenly', marginVertical: 10 }]}>
                    <View style={[{ flex: 1, flexDirection: 'column' }]}>
                      <Text style={styles.riverStatsHeader}>Gauge Height (ft):</Text>
                      <Text style={styles.riverStatsHeader}>{this.props.gauge}</Text>
                    </View>
                  </View>
                </View>
              </View>
              <Text>
                {'\n'}
                {'\n'}
                {'\n'}
              </Text>
              {/* This is where the graph will go once the proper functionality is implemented. Image is just a placeholder. */}
              <Image source={graphImage} />
              {/* The following view is what is shown when the menu is pulled up */}
              <View
                opacity={this.state.isHidden}
                style={styles.riverInfoContainer}
              >
                <Text style={styles.riverNameHeader}>{this.props.name}</Text>
                <Text style={styles.riverLocationHeader}>{this.props.region}</Text>
                <View style={[{ flexDirection: 'row', alignItems: 'center' }]}>

                  <View style={[{ flex: 1, flexDirection: 'column' }]}>
                    <Text style={styles.riverStatsHeader}>Discharge (ft3/s):</Text>
                    <Text style={styles.riverStatsHeader}>{this.props.discharge}</Text>
                  </View>

                  <View style={[{ justifyContent: 'space-evenly', marginVertical: 0 }]}>
                    <View style={[{ flex: 1, flexDirection: 'column' }]}>
                      <Text style={styles.riverStatsHeader}>Gauge Height (ft):</Text>
                      <Text style={styles.riverStatsHeader}>{this.props.gauge}</Text>
                    </View>
                  </View>

                </View>
              </View>
            </View>
          </View>
        </Animated.View>
      </Animated.View>
    );
  }
}

export default DraggableView;
