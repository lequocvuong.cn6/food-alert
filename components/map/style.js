import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');
const { height } = Dimensions.get('window');

export default StyleSheet.create({
  riverNameHeader: {
    color: '#C3865D',
    fontSize: 30,
    fontWeight: 'bold',
  },

  riverLocationHeader: {
    color: '#C3865D',
    fontSize: 24,
  },

  riverStatsHeader: {
    color: '#C3865D',
    fontSize: 18,
  },

  riverInfoContainer: {
    height: 150,
    paddingHorizontal: 10,
    paddingVertical: 60,
    alignItems: 'center',
    backgroundColor: 'white',
  },

  map: {
    height: height - (height / 15),
    // ...StyleSheet.absoluteFillObject
  },
  locationButtonContainer: {
    position: 'absolute',
    bottom: 0,
    left: width * 0.86,
    backgroundColor: 'transparent',
  },

  filterButtonContainer: {
    position: 'absolute',
    bottom: height * 0.05,
    left: width * 0.86,
    backgroundColor: 'transparent',

  },

  bookmarkButtonContainer: {
    position: 'absolute',
    bottom: height * 0.70,
    left: width * 0.90,
    backgroundColor: 'transparent',

  },

  // style POIs information
  topHeader: {
    position: 'absolute',
    // top: 30,
    flex: 1,
    width,
    backgroundColor: 'white',
  },

  header: {
    flexDirection: 'row',
    paddingVertical: 40,
  },

  iconFavor: {
    width: 24,
    height: 24,
  },

  animatedView: {
    position: 'absolute',
    backgroundColor: 'white',
    left: 0,
    right: 0,
    zIndex: 20,
    top: 0,
    height,
    flex: 1,
    flexDirection: 'row',
  },

});
