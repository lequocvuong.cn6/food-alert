import 'react-native-gesture-handler';
import * as React from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import Slider from '@react-native-community/slider';

const { width } = Dimensions.get('window');
const { height } = Dimensions.get('window');

const styles = StyleSheet.create({
  slider: {
    transform: [{ rotate: '90deg' }],
    position: 'absolute',
    width: (width * 19) / 17,
    top: height / 1.85,
    left: width / 2.85,
  },
});
class ZoomSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.startingZoom,
    };
  }

  handleChange = (value) => {
    this.setState({
      value,
    });
    this.props.updateZoomLevel(this.state.value);
  }

  render() {
    return (
      <View style={styles.slider}>
        <Slider
          inverted
          minimumTrackTintColor="#5E96AE"
          maximumTrackTintColor="#67B36F"
          thumbTintColor="#B2EBE0"
          value={this.state.value}
          step={0.05}
          maximumValue={10}
          minimumValue={0}
          onValueChange={this.handleChange}
          onSlidingStart={this.handleChange}
          onSlidingComplete={this.handleChange}
        />
      </View>
    );
  }
}

export default ZoomSlider;
