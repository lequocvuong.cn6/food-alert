import React, { Component } from 'react';
import {
    View, TextInput,
  } from 'react-native';
import styles from './style';
  export const CredentialsFields = class CredentialsFields extends Component {
    constructor(props) {
        super(props);
        this.state = {
            credentials:{ username:'', password:'' }  
        };
    }
    sendCredentials = ( ) => {
        this.props.parentCredentials(this.state.credentials)
    }
    getUsername = (username) => {
        this.setState({username})
    };
    getPassword = (password) => {
        this.setState({password})
    };
    
    render(){
        return(
            <View>
                <UsernameField
                    parentUsername = {this.getUsername}
                />
                <PasswordField
                    parentPassword ={this.getPassword}
                />
            </View>
        )
    }
    
}
class UsernameField extends Component {
    constructor(props) {
        super(props);
        this.state = {username: ''}
    }
    sendUsername = () =>{
        this.props.parentUsername(this.state.username)
    }
    render(){
        return(
            <View style={styles.field}>
              <TextInput
                style={styles.placeholder}
                placeholder="Username"
                onChangeText={(username) => this.setState({ username })}
                value={this.state.username}
              />
          </View>
        )
    }
}
class PasswordField extends Component {
    constructor(props) {
        super(props);
        this.state = {password: ''}
    }
    sendPassword = () => {
        this.props.parentPassword(this.state.password)
    }
    render(){
        return(
            <View style={styles.field}>
            <TextInput style={styles.placeholder}
              placeholder="Password"
              onChangeText={(password) => this.setState({ password })}
              value={this.state.password}
              secureTextEntry={true}
            />
          </View>
        )
    }
}

