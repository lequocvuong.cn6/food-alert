import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');
export default StyleSheet.create({
  title: {
    textAlign: 'center',
    // justifyContent: 'center',
    fontSize: 50,
    color: '#F57460',
    margin: '5%',
  },
  placeholder: {
    textAlign: 'justify',
    marginLeft: '5%',
  },
  field: {
    flex: 0,
    justifyContent: 'center',
    marginBottom: '5%',
    paddingVertical: '1%',
    borderRadius: 10,
    borderWidth: 0,
    backgroundColor: 'white',
    minWidth: '75%',
    maxWidth: '75%',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tinyLogo: {
    width: '33%',
    height: 0.33 * width,

  },
  imgBackground: {
    width: 'auto',
    height: 'auto',
    flex: 1,
  },
  button: {
    margin: '2%',
    fontSize: 20,
    paddingHorizontal: '10%',
    paddingVertical: '2.5%',
    borderRadius: 10,
    backgroundColor: 'white',
  },
});
