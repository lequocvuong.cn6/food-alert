import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');
const specialText = '#F57460';
export default StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: 50,
    color: specialText,
    marginTop: '10%',
  },

  subtitle: {
    textAlign: 'center',
    fontSize: 12,
    color: specialText,
    margin: '1%', 
  },

  field: {
    flex: 1,
    justifyContent: 'center',
    height: '5%',
    paddingHorizontal: '25%',
    borderRadius: 10,
    borderWidth: 1,
    backgroundColor: 'white',
  },
  main: {
    flex: 1,
    alignItems: 'center',
  },

  imgBackground: {
    width: 'auto',
    height: 'auto',
    flex: 1,
  },
  resultButton: {
    margin: '2%',
    fontSize: 20,
    paddingHorizontal: '10%',
    paddingVertical: '2.5%',
    borderRadius: 10,
    backgroundColor: 'white',
  },
  specialText: {
    color: specialText
  },
  footer: {
    alignItems: 'center',
    bottom: '5%'
  },

  // style watchlist
  list: {
    paddingHorizontal: 10,
  },
});
