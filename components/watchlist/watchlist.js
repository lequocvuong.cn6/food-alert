import React, { Component } from 'react';
import {
  Text, View, ImageBackground, TouchableOpacity,
} from 'react-native';
import * as SQLite from 'expo-sqlite';
import Feather from 'react-native-vector-icons/Feather';
import { FlatList } from 'react-native-gesture-handler';
import styles from './style';
import SwipeableRow from './SwipeableRow';

const db = SQLite.openDatabase('database.db');

const background = require('../../assets/background.png');

class Watchlist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    // this.getData();

    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      // do something
      console.log('focus Watchlist');
      this.getData();
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

    getData = () => {
      db.transaction(
        (tx) => {
          tx.executeSql('select * from watchlist', [], (_, { rows }) => {
            console.log(JSON.stringify(rows));
            this.setState({
              data: rows._array,
            });
          });
        },
        (e) => {
          console.log(e);
        },
      );
    }

    navigateMap = (item) => {
      this.props.navigation.navigate('Map', { item });
    }

    // Pressing a result currently just shows the map..work in progress
    Result=async () => {
      this.props.navigation.navigate('Map');
    }

    onDelete = (item) => {
      console.log('item');
      console.log(item);
      db.transaction(
        (tx) => {
          tx.executeSql('delete from watchlist where id = ?;', [item.id]);
          tx.executeSql('select * from watchlist', [], (_, { rows }) => {
            console.log(JSON.stringify(rows));
            this.setState({
              data: rows._array,
            });
          });
        },
        (e) => {
          console.log(e);
        },
      );
    }

    render() {
      return (

        <View style={{ flex: 1 }}>

          <ImageBackground
            style={styles.imgBackground}
            resizeMode="cover"
            source={background}
          >
            <View style={styles.main}>
              <Text style={styles.title}>Watchlist</Text>
              <Text style={styles.subtitle}>View your bookmarked gages:</Text>

              {/* For every search result, have a button that can be clicked */}
              <View>
                <FlatList
                  style={styles.list}
                  data={this.state.data}

                  keyExtractor={(item, key) => key.toString()}
                  renderItem={({ item }) => (
                    <SwipeableRow onDelete={() => this.onDelete(item)}>
                      <View style={{
                        backgroundColor: 'white', paddingVertical: 15, paddingHorizontal: 5, marginTop: 10,
                      }}
                      >
                        <TouchableOpacity onPress={() => this.navigateMap(item)}>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={{ color: '#CC9966', fontWeight: 'bold', fontSize: 18 }}>{item.siteName}</Text>
                            <Feather
                              name="chevron-right"
                              size={26}
                              color="#CC9966"
                            />
                          </View>

                        </TouchableOpacity>
                      </View>
                    </SwipeableRow>
                  )}
                />
              </View>
            </View>
          </ImageBackground>
        </View>
      );
    }
}

export default Watchlist;
