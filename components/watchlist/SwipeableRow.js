import React, { Component } from 'react';
import {
  Animated, StyleSheet, I18nManager,
} from 'react-native';

import { RectButton, Swipeable } from 'react-native-gesture-handler';

import Icon from 'react-native-vector-icons/MaterialIcons';

const AnimatedIcon = Animated.createAnimatedComponent(Icon);

const styles = StyleSheet.create({
  leftAction: {
    flex: 1,
    backgroundColor: '#388e3c',
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexDirection: I18nManager.isRTL ? 'row' : 'row-reverse',
  },
  actionIcon: {
    width: 30,
    marginHorizontal: 10,
  },
  rightAction: {
    alignItems: 'center',
    //   flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row',
    flexDirection: I18nManager.isRTL ? 'row' : 'row-reverse',
    backgroundColor: '#dd2c00',
    flex: 1,
    justifyContent: 'flex-end',
  },
});

export default class SwipeableRow extends Component {
  renderLeftActions = () => (
    <RectButton style={styles.rightAction} onPress={this.close}>
      <AnimatedIcon
        name="delete-forever"
        size={30}
        color="#fff"
        style={styles.actionIcon}
      />
    </RectButton>
  );

  renderRightActions = () => (
    <RectButton style={styles.rightAction} onPress={this.close}>
      <AnimatedIcon
        name="delete-forever"
        size={30}
        color="#fff"
        style={[styles.actionIcon]}
      />
    </RectButton>
  );

  updateRef = (ref) => {
    this.swipeableRow = ref;
  };

  close = () => {
    this.swipeableRow.close();
    this.props.onDelete();
  };

  render() {
    const { children } = this.props;
    return (
      <Swipeable
        ref={this.updateRef}
        friction={2}
        leftThreshold={80}
        rightThreshold={41}
        renderLeftActions={this.renderLeftActions}
      >
        {children}
      </Swipeable>
    );
  }
}

SwipeableRow.defaultProps = {
  onDelete() {
  },

};
