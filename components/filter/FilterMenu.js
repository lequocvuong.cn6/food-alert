import * as React from 'react';
import RadioForm from 'react-native-simple-radio-button';
import { Text, View, Modal } from 'react-native';
import { Button, ButtonGroup } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from './style';

const INDEX_0 = 0;
const INDEX_1 = 1;
const INDEX_2 = 2;
const INDEX_3 = 3;
const INDEX_4 = 4;
const INDEX_5 = 5;

const depthRadioProps = [
  { label: '< 25', value: INDEX_0 },
  { label: '25-50', value: INDEX_1 },
  { label: '50-75', value: INDEX_2 },
  { label: '75-100', value: INDEX_3 },
  { label: '100-125', value: INDEX_4 },
  { label: '125 <', value: INDEX_5 },
];

const altitudeRadioProps = [
  { label: '< 500', value: INDEX_0 },
  { label: '500-750', value: INDEX_1 },
  { label: '750-1000', value: INDEX_2 },
  { label: '1000-1250', value: INDEX_3 },
  { label: '1250-1500', value: INDEX_4 },
  { label: '1500 <', value: INDEX_5 },
];

const typeRadioProps = [
  { label: 'Well', value: INDEX_0 },
  { label: 'Test Hole', value: INDEX_1 },
  { label: 'Excavation', value: INDEX_2 },
  { label: 'Sinkhole', value: INDEX_3 },
  { label: 'Spring', value: INDEX_4 },
];

export default class FilterMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
    };
  }

  setDepthFilterValue(value) {
    let filterString = '';
    if (value === INDEX_0) {
      filterString = '&wellDepthMax=25';
    } else if (value === INDEX_1) {
      filterString = '&wellDepthMin=25&wellDepthMax=50';
    } else if (value === INDEX_2) {
      filterString = '&wellDepthMin=50&wellDepthMax=75';
    } else if (value === INDEX_3) {
      filterString = '&wellDepthMin=75&wellDepthMax=100';
    } else if (value === INDEX_4) {
      filterString = '&wellDepthMin=100&wellDepthMax=125';
    } else if (value === INDEX_5) {
      filterString = '&wellDepthMin=125';
    }
    this.props.filterCallback(filterString);
  }

  setAltitudeFilterValue(value) {
    let filterString = '';
    if (value === INDEX_0) {
      filterString = '&wellDepthMax=500';
    } else if (value === INDEX_1) {
      filterString = '&wellDepthMin=500&wellDepthMax=750';
    } else if (value === INDEX_2) {
      filterString = '&wellDepthMin=750&wellDepthMax=1000';
    } else if (value === INDEX_3) {
      filterString = '&wellDepthMin=1000&wellDepthMax=1250';
    } else if (value === INDEX_4) {
      filterString = '&wellDepthMin=1250&wellDepthMax=1500';
    } else if (value === INDEX_5) {
      filterString = '&wellDepthMin=1500';
    }
    this.props.filterCallback(filterString);
  }

  setTypeFilterValue(value) {
    let filterString = '';
    if (value === INDEX_0) {
      filterString = '&siteType=GW';
    } else if (value === INDEX_1) {
      filterString = '&siteType=GW-TH';
    } else if (value === INDEX_2) {
      filterString = '&siteType=LA-EX';
    } else if (value === INDEX_3) {
      filterString = '&siteType=LA-SNK';
    } else if (value === INDEX_4) {
      filterString = '&siteType=SP';
    }
    this.props.filterCallback(filterString);
  }

  updateIndex = (selectedIndex) => {
    this.setState({ selectedIndex });
  }

  removeFilter = () => {
    this.props.filterCallback('');
  }

  render() {
    const buttons = ['TYPE', 'ALTITUDE', 'DEPTH'];
    const { selectedIndex } = this.state;
    return (
      <Modal transparent visible={this.props.isVisible} animationType="fade">
        <View style={styles.transparentBackground}>
          <View style={styles.filterWindow}>
            <View style={styles.filterWindowContainer}>
              <Text style={styles.filterWindowTitle}>
                Filter
              </Text>
              <Button
                onPress={() => {
                  this.props.visibilityCallback(false);
                }}
                buttonStyle={{ backgroundColor: 'transparent' }}
                icon={(
                  <Ionicons
                    name="md-close-circle"
                    size={30}
                  />
                )}
              />
            </View>
            <ButtonGroup
              onPress={this.updateIndex}
              selectedIndex={selectedIndex}
              buttons={buttons}
              selectedTextStyle={styles.selectedTextStyle}
              selectedButtonStyle={{ backgroundColor: 'white' }}
              textStyle={{ fontSize: 16 }}
            />
            <View  style={styles.radioFormContainer}>
              {this.state.selectedIndex === 0 && (
                <RadioForm
                  radio_props={typeRadioProps}
                  onPress={(value) => {
                    this.setTypeFilterValue(value);
                  }}
                />
              )}
              {this.state.selectedIndex === 1 && (
                <RadioForm
                  radio_props={altitudeRadioProps}
                  onPress={(value) => {
                    this.setAltitudeFilterValue(value);
                  }}
                />
              )}
              {this.state.selectedIndex === 2 && (
                <RadioForm
                  radio_props={depthRadioProps}
                  onPress={(value) => {
                    this.setDepthFilterValue(value);
                  }}
                />
              )}
            </View>

            <View style={styles.container}>
              <View style={styles.buttonContainer}>
                <Button
                  onPress={() => {
                    this.removeFilter();
                    this.props.visibilityCallback(false);
                  }}
                  title="CLEAR"
                  titleStyle={{ fontSize: 20, color: 'black' }}
                  buttonStyle={styles.buttonStyle}
                  type="outline"
                />
              </View>
              <View style={styles.buttonContainer}>
                <Button
                  onPress={() => {
                    this.props.visibilityCallback(false);
                  }}
                  title="APPLY"
                  titleStyle={{ fontSize: 20 }}
                  buttonStyle={styles.buttonStyle}
                />
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
