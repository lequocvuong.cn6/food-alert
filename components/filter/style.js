import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    paddingHorizontal: '2%',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 15,
  },

  buttonContainer: {
    flex: 1,
    paddingHorizontal: '2%',
  },

  buttonStyle: {
    borderWidth: 1,
    paddingHorizontal: '12%',
    paddingVertical: '9%',
  },

  radioFormContainer: {
    paddingTop: '5%',
    paddingLeft: '7%',
    flex: 1
  },

  transparentBackground: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    flex: 1,
  },

  filterWindow: {
    backgroundColor: 'white',
    marginVertical: '25%',
    marginHorizontal: '5%',
    borderRadius: 15,
    flex:1
  },

  filterWindowContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: '3%',
  },

  filterWindowTitle: {
    fontSize: 45,
    fontWeight: 'bold',
    paddingTop: '1%',
    paddingLeft: '5%',
  },

  selectedTextStyle: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 20,
  },
});
