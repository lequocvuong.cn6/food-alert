import { StyleSheet, Dimensions } from 'react-native';

const specialText = '#F57460';
const { height } = Dimensions.get('window');
const { width } = Dimensions.get('window');

export default StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent',
    color: specialText,
    marginTop: '10%',
  },

  header: {
    textAlign: 'center',
    fontSize: 20,
    backgroundColor: 'transparent',
    color: specialText,
    marginBottom: 20,
  },
  optionContainerView: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  optionLabelView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    padding: 10,
  },
  imgBackground: {
    width: 'auto',
    height: 'auto',
    flex: 1,
  },
  specialText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: specialText,
  },
  buttonContainerView: {
    paddingBottom: 5,
    paddingTop: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'transparent',
  },
  button: {
    backgroundColor: 'transparent',
    color: specialText,
  },
  buttonTitleStyle: {
    color: specialText,
    fontSize: 16,
    fontWeight: 'bold',
  },
  policyView: {
    position: 'relative',
    zIndex: 10,
    backgroundColor: 'white',
    alignSelf: 'center',
    height: height * 0.75,
    width: width * 0.85,
    bottom: (height - (height * 0.85)) / 2,
  },
  backButton: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
  },
  policyText: {
    color: '#81b8ad',
    fontSize: 12,
    textAlign: 'justify',
    padding: 10,
  },
});
