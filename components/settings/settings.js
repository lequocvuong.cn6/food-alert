import 'react-native-gesture-handler';
import React from 'react';
import {
  View, Text, ImageBackground, Switch, ScrollView,
} from 'react-native';
import { Button } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from './style';

const iconColor = '#F57460';
const iconSize = 30;
const toggleThumbColor = '#f4f3f4';
const toggleInActiveTrackColor = '#F57460';
const toggleActiveTrackColor = '#767577';
const policy = require('../../assets/policy.json');
const background = require('../../assets/background.png');

class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true, // need to check if user logged in (to be implemented once login functionality is created)
      policyBtnClicked: false,
      darkModeEnabled: false,
      noficationsMuted: false,
    };
  }

  logout=async () => {
    // need to implement log out functionality here
    this.setState((prevstate) => ({
      loggedIn: !prevstate.loggedIn,
    }));
    this.props.navigation.navigate('Map');
  }

  login=async () => {
    this.setState((prevstate) => ({
      loggedIn: !prevstate.loggedIn,
    }));
    this.props.navigation.pop(); // redirects to login page
  }

  toggleDarkMode() {
    this.setState((prevstate) => ({
      darkModeEnabled: !prevstate.darkModeEnabled,
    }));
  }

  toggleNotifications() {
    this.setState((prevstate) => ({
      noficationsMuted: !prevstate.noficationsMuted,
    }));
  }

  render() {
    return (
      <>
        <View style={{ flex: 1 }}>
          <ImageBackground
            style={styles.imgBackground}
            resizeMode="cover"
            source={background}
          >
            <Text style={styles.title}>Settings</Text>
            <Text style={styles.header}>Change the settings to your liking</Text>

            {/* view that is on top of the other views inside the parent view
                hidden by default on shown when privacy button is pressed.
            */}
            <View style={[styles.policyView, { display: this.state.policyBtnClicked ? 'flex' : 'none' }]}>
              <Ionicons
                onPress={() => {
                  this.setState((prevstate) => ({
                    policyBtnClicked: !prevstate.policyBtnClicked,
                  }));
                }}
                name="md-arrow-back"
                size={iconSize}
                color={iconColor}
                style={styles.icon}
              />
              <ScrollView>
                <Text style={styles.policyText}>{policy.policy}</Text>
              </ScrollView>
            </View>

            {/* dark mode setting row */}
            <View style={styles.optionContainerView}>
              <View style={styles.optionLabelView}>
                <Ionicons
                  name="md-eye"
                  size={iconSize}
                  color={iconColor}
                  style={styles.icon}
                />
                <Text style={styles.specialText}>Dark Mode</Text>
              </View>
              <Switch
                onValueChange={() => this.toggleDarkMode()}
                trackColor={{ false: toggleActiveTrackColor, true: toggleInActiveTrackColor }}
                thumbColor={toggleThumbColor}
                value={this.state.darkModeEnabled}
                style={{ justifyContent: 'flex-end' }}
              />
            </View>

            {/* mute notifications setting row */}
            <View style={styles.optionContainerView}>
              <View style={styles.optionLabelView}>
                <Ionicons
                  name="md-notifications"
                  size={iconSize}
                  color={iconColor}
                  style={styles.icon}
                />
                <Text style={styles.specialText}>Notifications</Text>
              </View>
              <Switch
                onValueChange={() => this.toggleNotifications()}
                trackColor={{ false: toggleActiveTrackColor, true: toggleInActiveTrackColor }}
                thumbColor={toggleThumbColor}
                value={this.state.noficationsMuted}
                style={{ justifyContent: 'flex-end' }}
              />
            </View>

            {/* privacy policy setting row */}
            <View style={styles.buttonContainerView}>
              <Button
                onPress={() => {
                  this.setState((prevstate) => ({
                    policyBtnClicked: !prevstate.policyBtnClicked,
                  }));
                }}
                titleStyle={styles.buttonTitleStyle}
                title="Privacy policy"
                buttonStyle={styles.button}
                icon={(
                  <Ionicons
                    name="md-lock"
                    size={iconSize}
                    color={iconColor}
                    style={styles.icon}
                  />
              )}
              />
            </View>

            {/* mail us setting row */}
            <View style={styles.buttonContainerView}>
              <Button
                titleStyle={styles.buttonTitleStyle}
                title="E-Mail Us"
                buttonStyle={styles.button}
                icon={(
                  <Ionicons
                    name="md-mail"
                    size={iconSize}
                    color={iconColor}
                    style={styles.icon}
                  />
              )}
              />
            </View>

            {/* logout/login setting row */}
            <View style={[styles.buttonContainerView, { display: this.state.loggedIn ? 'flex' : 'none' }]}>
              <Button
                accessibilityLabel="Logout"
                onPress={() => {
                  this.logout();
                }}
                titleStyle={styles.buttonTitleStyle}
                title="Logout"
                buttonStyle={styles.button}
                icon={(
                  <Ionicons
                    name="md-log-out"
                    size={iconSize}
                    color={iconColor}
                    style={styles.icon}
                  />
              )}
              />
            </View>

            <View style={[styles.buttonContainerView, { display: this.state.loggedIn ? 'none' : 'flex' }]}>
              <Button
                accessibilityLabel="Login"
                onPress={() => {
                  this.login();
                }}
                titleStyle={styles.buttonTitleStyle}
                title="Login"
                buttonStyle={styles.button}
                icon={(
                  <Ionicons
                    name="md-log-in"
                    size={iconSize}
                    color={iconColor}
                    style={styles.icon}
                  />
              )}
              />
            </View>

          </ImageBackground>
        </View>
      </>
    );
  }
}
export default Settings;
