import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Homescreen from '../Homescreen';

configure({ adapter: new Adapter() });

// Renders Homescreen components without crashing
describe('Rendering component test with Enzyme', () => {
  it('renders homescreen without crashing', async () => {
    shallow(<Homescreen />);
  });
});
