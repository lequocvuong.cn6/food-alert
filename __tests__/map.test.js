import React from 'react';
import renderer from 'react-test-renderer';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Map from '../components/map/map';

configure({ adapter: new Adapter() });

// Tests Map component rendering
describe('Map is rendered correctly', () => {
  it('Map is displayed', async () => {
    const tree = renderer.create(<Map />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

// Tests initial state of the map location
it('Testing state', () => {
  const startState = {
    sites: [],
    region: {
      latitude: 45.536060,
      longitude: -73.657548,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0922,
    },
    modalVisible: false,
    itemPoi: {
      lat: 0,
      lon: 0,
      siteName: '',
    },
    isFavor: false,
    siteDischarge: '0',
    siteGauge: '0',
    siteName: 'No gage has been selected',
    siteRegion: '',
  };

  const wrap = shallow(<Map />);
  const instance = wrap.instance();
  expect(instance.state).toEqual(startState);
});

// tests button press to get new current location
it('Testing button press', async () => {
  const wrap = shallow(<Map />);
  const button = wrap.find('.button');
  expect(wrap.find('.button').exists()).toEqual(true);

  button.first().simulate('press');
});
