import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import FetchLocation from '../components/FetchLocation';

configure({ adapter: new Adapter() });

// Tests rendering of Fetchlocation component
describe('Rendering component test with Enzyme', () => {
  it('renders homescreen without crashing', async () => {
    shallow(<FetchLocation />);
  });
});

// Tests onPress change handler of Button in FetchLocation component
describe('Test button onPress function rendering', () => {
  const wrapper = shallow(<FetchLocation />);
  const button = wrapper.find({ title: 'Get location' });
  expect(wrapper.find({ title: 'Get location' }).exists()).toEqual(true);
  button.first().simulate('press');
});
