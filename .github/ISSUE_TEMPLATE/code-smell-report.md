---
name: Code Smell report
about: Create a report to help us improve
title: CS-#
labels: Code smells, Quality metrics, Refactoring
assignees: ''

---

**Reported by**: SonarQube / Name of person creating bug report
**Submission date**:
## **SonarQube report Details**
**Path**:
**Lines**:
**Defect Severity**: 
**Summary**:
**Non compliant example**:
|```````````````
INSERT
|```````````````

**Compliant example**:
|```````````````
INSERT
|`````````
