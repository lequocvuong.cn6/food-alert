---
name: Quality defect (bugs detected on SonarQube)
about: Create a report to help us improve
title: QD-#
labels: Bug, Quality metrics
assignees: ''

---

**Reported by**: SonarQube / Name of person creating bug report
**Submission date**:
## **SonarQube report Details**
**Path**:
**Lines**:
**Defect Severity**: 
**Summary**:
**Non compliant example**:
|```````````````
INSERT
|```````````````

**Compliant example**:
|```````````````
INSERT
|`````````