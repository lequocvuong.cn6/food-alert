---
name: Standard bug report
about: Create a report to help us improve
title: 'D-# '
labels: Bug
assignees: ''

---

**ID**:
**Name**:
**Related to issue**:
**Reported by**:
**Submitted**:
## **Environment**
**Device**:
## **Details**
**Summary**:
**Steps to reproduce**:
**Expected results**:
**Actual results**:
## **Severity**
**Severity**:
**Priority**:

NOTE: Add screenshots or gifs if applicable
